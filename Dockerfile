FROM openjdk:17-jdk-alpine
FROM gradle:latest
COPY . .
RUN gradle build
RUN ls
RUN cp build/libs/server-0.0.1-SNAPSHOT.jar lambda_updateserver.jar
ENTRYPOINT ["java", "-jar", "lambda_updateserver.jar"]

