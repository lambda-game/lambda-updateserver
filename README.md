# Lambda update server
Handles updates for the lambda client & lambda launcher

## Flow
1. Developer uploads new version
2. Launcher asks update server for news
3. Launcher asks update server for list of versions
4. Launcher downloads version from the jarUrl
5. Launcher launches