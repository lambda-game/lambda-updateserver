package com.interfiber.lambda.updater.server.controllers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerInfoController {
    @GetMapping("/server")
    public String serverInfo(){
        return "LambdaUpdateServer(Spring) v1.0";
    }
}
