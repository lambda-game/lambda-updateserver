package com.interfiber.lambda.updater.server.requests;

public class UploadNewsArticleRequest {
    public String title;
    public String content;
    public String token;
}
