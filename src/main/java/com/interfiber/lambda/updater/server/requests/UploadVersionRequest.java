package com.interfiber.lambda.updater.server.requests;

public class UploadVersionRequest {
    public boolean hidden;
    public String name;
    public String jarUrl;
    public String token;
}
