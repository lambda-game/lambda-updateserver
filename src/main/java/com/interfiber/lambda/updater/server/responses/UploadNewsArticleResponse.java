package com.interfiber.lambda.updater.server.responses;

public class UploadNewsArticleResponse {
    public String message;
    public boolean error;
}
