package com.interfiber.lambda.updater.server.db;

public class VersionInfo {
    public String versionName;
    public String releaseDate;
    public String jarUrl;
}
