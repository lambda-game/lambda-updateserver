package com.interfiber.lambda.updater.server.controllers;


import com.interfiber.lambda.updater.server.UpdateServerApplication;
import com.interfiber.lambda.updater.server.db.UpdateServerPG;
import com.interfiber.lambda.updater.server.requests.PullVersionRequest;
import com.interfiber.lambda.updater.server.requests.SetLatestVersionRequest;
import com.interfiber.lambda.updater.server.requests.UploadVersionRequest;
import com.interfiber.lambda.updater.server.responses.PullVersionResponse;
import com.interfiber.lambda.updater.server.responses.UploadVersionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UploadVersionController {

    static Logger logger = LoggerFactory.getLogger(UpdateServerApplication.class);

    @PostMapping("/api/v1/upload/uploadVersion")
    public UploadVersionResponse uploadVersion(@RequestBody UploadVersionRequest body){
        if (body.jarUrl == null || body.token == null){
            UploadVersionResponse error = new UploadVersionResponse();
            error.message = "missing data";
            error.error = true;
            return error;
        }
        logger.info("uploading new version");
        return UpdateServerPG.createVersion(body);
    }

    @PostMapping("/api/v1/upload/pullVersion")
    public PullVersionResponse pullVersion(@RequestBody PullVersionRequest body){
        if (body.token == null || body.name == null){
            PullVersionResponse response = new PullVersionResponse();
            response.error = true;
            response.message = "missing data";
            return response;
        }
        logger.info("pulling version from database");
        return UpdateServerPG.pullVersion(body);
    }

    @PostMapping("/api/v1/upload/setLatestVersion")
    public boolean setLatest(@RequestBody SetLatestVersionRequest body){
        if (body.token == null || body.latestVersion == null){
            return false;
        }

        return UpdateServerPG.setLatestVersion(body);
    }
}
