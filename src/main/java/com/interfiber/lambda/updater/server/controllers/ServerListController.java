package com.interfiber.lambda.updater.server.controllers;

import com.interfiber.lambda.updater.server.responses.BroadcastServerResponse;
import com.interfiber.lambda.updater.server.serverlist.ServerInfo;
import com.interfiber.lambda.updater.server.serverlist.ServerList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@RestController
public class ServerListController {
    @PostMapping("/api/v1/serverlist/broadcast")
    public BroadcastServerResponse broadcast(@RequestBody ServerInfo info, HttpServletRequest request){


        BroadcastServerResponse response = new BroadcastServerResponse();

        if (info.serverDesc == null || info.serverIcon == null || info.serverName == null){
            response.error = true;
            response.message = "missing data";

            return response;
        }

        if (!info.serverIcon.endsWith(".png")){
            response.error = true;
            response.message = "server info url must end with .png";
        }

        if (info.serverIp == null){
            response.error = true;
            response.message = "missing ip address";

            return response;
        }

        ServerList.serverInfoList.add(info);

        response.message = "Added server";

        return response;
    }

    @GetMapping("/api/v1/serverlist/list")
    public ArrayList<ServerInfo> list(){
        return ServerList.serverInfoList;
    }
}
