package com.interfiber.lambda.updater.server.db;

public class NewsArticle {
    public String title;
    public String content;
    public String date;
}
