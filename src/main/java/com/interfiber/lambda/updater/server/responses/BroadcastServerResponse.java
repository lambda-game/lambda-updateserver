package com.interfiber.lambda.updater.server.responses;

public class BroadcastServerResponse {
    public String message;
    public boolean error;
}
