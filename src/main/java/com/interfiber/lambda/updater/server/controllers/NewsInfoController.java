package com.interfiber.lambda.updater.server.controllers;

import com.interfiber.lambda.updater.server.db.NewsArticle;
import com.interfiber.lambda.updater.server.db.UpdateServerPG;
import com.interfiber.lambda.updater.server.requests.UploadNewsArticleRequest;
import com.interfiber.lambda.updater.server.responses.UploadNewsArticleResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class NewsInfoController {
    @GetMapping("/api/v1/news/getNews")
    public ArrayList<NewsArticle> getNews(){
        ArrayList<NewsArticle> newsList = UpdateServerPG.getNews();
        return newsList;
    }

    @PostMapping("/api/v1/news/uploadNews")
    public UploadNewsArticleResponse uploadNews(@RequestBody UploadNewsArticleRequest body){
        if (body.content == null || body.token == null){
            UploadNewsArticleResponse response = new UploadNewsArticleResponse();
            response.error = true;
            response.message = "missing data";
            return response;
        }

        return UpdateServerPG.createNewsArticle(body);
    }
}
