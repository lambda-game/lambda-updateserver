package com.interfiber.lambda.updater.server.responses;

public class UploadVersionResponse {
    public boolean error;
    public String message;
}
