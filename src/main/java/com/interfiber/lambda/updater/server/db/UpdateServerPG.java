package com.interfiber.lambda.updater.server.db;

import com.interfiber.lambda.updater.server.SqlUtils;
import com.interfiber.lambda.updater.server.UpdateServerApplication;
import com.interfiber.lambda.updater.server.requests.PullVersionRequest;
import com.interfiber.lambda.updater.server.requests.SetLatestVersionRequest;
import com.interfiber.lambda.updater.server.requests.UploadNewsArticleRequest;
import com.interfiber.lambda.updater.server.requests.UploadVersionRequest;
import com.interfiber.lambda.updater.server.responses.PullVersionResponse;
import com.interfiber.lambda.updater.server.responses.UploadNewsArticleResponse;
import com.interfiber.lambda.updater.server.responses.UploadVersionResponse;
import com.interfiber.lambda.updater.server.security.UploadAccess;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

public class UpdateServerPG {
    public static Connection connection;
    static Logger logger = LoggerFactory.getLogger(UpdateServerApplication.class);

    public static void init(){
        logger.info("connecting to database...");
        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setApplicationName("lambdaUpdateServer");
        ds.setSslMode("require");
        ds.setUser("authroot");

        ds.setPassword(Optional.ofNullable(System.getenv("UPDATE_DB_PASSWORD")).orElseThrow(() -> new IllegalArgumentException("UPDATE_DB_PASSWORD not set")));
        ds.setUrl(Optional.ofNullable(System.getenv("UPDATE_DB_URL")).orElseThrow(
                () -> new IllegalArgumentException("UPDATE_DB_URL is not set.")));

        try {
            connection = ds.getConnection();
        } catch (SQLException e) {
            logger.error(e.toString());
            logger.error("failed to connect to update server database");
            System.exit(-1);
        }
    }

    public static void execute(String stmt) {
        try {
            Statement st = connection.createStatement();
            boolean rs = st.execute(stmt);
            st.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> executeQuery(String queryString){
        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery(queryString);
            ArrayList<String> result = new ArrayList<>();
            while (set.next()){
                result.add(set.getString(1));
            }
            set.close();
            st.close();
            return result;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void createDefaults(){
        String[] statements = {"CREATE TABLE IF NOT EXISTS versions (id UUID PRIMARY KEY DEFAULT gen_random_uuid(), versionName STRING, releaseDate STRING, hidden STRING, jarUrl STRING);", "CREATE TABLE IF NOT EXISTS uploadTokens(token String);", "create table if not exists news(title STRING, content STRING, uploadDate STRING);", "create table if not exists latestVersion(version STRING);"};

        execute(statements[0]);
    }

    public static ArrayList<VersionInfo> getVersionList(){
        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from versions where hidden='false';");

            ArrayList<VersionInfo> versionList = new ArrayList<>();

            while (set.next()){
                VersionInfo info = new VersionInfo();
                info.versionName = set.getString("versionName");
                info.jarUrl = set.getString("jarUrl");
                info.releaseDate = set.getString("releaseDate");

                versionList.add(info);
            }

            return versionList;
        } catch (SQLException e) {
            logger.error(e.toString());
            logger.error("failed to get version list");
            return null;
        }
    }

    public static VersionInfo getVersion(String name) {
        if (!SqlUtils.isSqlInjectionSafe(name)){
            logger.error("isSqlInjectionSafe(...) returned false");
            return null;
        }

        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from versions where versionName='" + name + "'");

            VersionInfo info;

            while (set.next()){
                info = new VersionInfo() ;
                info.releaseDate = set.getString("releaseDate");
                info.versionName = set.getString("versionName");
                info.jarUrl = set.getString("jarUrl");

                return info;
            }
        } catch (SQLException e){
            logger.error(e.toString());
            logger.error("failed to get version info");
            return null;
        }
        return null;
    }

    public static UploadVersionResponse createVersion(UploadVersionRequest body) {
        // check permission
        if (!UploadAccess.hasUploadAccess(body.token)){
            logger.error("no upload access");
            UploadVersionResponse uploadVersionResponse = new UploadVersionResponse();
            uploadVersionResponse.error = true;
            uploadVersionResponse.message = "No upload access";

            return uploadVersionResponse;
        } else {
           if (!SqlUtils.isSqlInjectionSafe(body.jarUrl)) {
               return null;
           }
           if (!SqlUtils.isSqlInjectionSafe(body.name)){
               return null;
           }

            try {
                Statement st = connection.createStatement();
                st.execute("insert into versions (versionName, releaseDate, hidden, jarUrl) values('" + body.name + "', '" + Instant.now().getEpochSecond() + "', '" + body.hidden + "', '" + body.jarUrl + "');");
                logger.info("inserted new version");

                UploadVersionResponse uploadVersionResponse = new UploadVersionResponse();
                uploadVersionResponse.message = "Uploaded version to database";
                uploadVersionResponse.error = false;
                return uploadVersionResponse;

            } catch (SQLException e) {
                logger.error(e.toString());
                logger.error("failed to upload version");
                UploadVersionResponse versionRequest = new UploadVersionResponse();
                versionRequest.error = true;
                versionRequest.message = "SQLException during upload, check server logs for trace";
                return versionRequest;
            }
        }
    }

    public static PullVersionResponse pullVersion(PullVersionRequest body){
        if (!UploadAccess.hasUploadAccess(body.token)){
            logger.error("no upload access");
            PullVersionResponse response = new PullVersionResponse();
            response.message = "No upload access";
            response.error = true;
            return response;
        }

        if (!SqlUtils.isSqlInjectionSafe(body.name)) {
            return null;
        }

        try {
            Statement st = connection.createStatement();
            st.execute("delete from versions where versionName='" + body.name + "'");

            PullVersionResponse pullVersionResponse = new PullVersionResponse();
            pullVersionResponse.error = false;
            pullVersionResponse.message = "Pulled version from database";

            return pullVersionResponse;
        } catch (SQLException e){
            logger.error(e.toString());
            logger.error("failed to pull version");

            PullVersionResponse pullVersionResponse = new PullVersionResponse();
            pullVersionResponse.message = "SQLException during pull, check server logs for trace";
            pullVersionResponse.error = true;

            return pullVersionResponse;
        }
    }

    public static ArrayList<NewsArticle> getNews(){
        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from news;");

            ArrayList<NewsArticle> newsList = new ArrayList<>();

            while (set.next()){
                NewsArticle article = new NewsArticle();
                article.content = set.getString("content");
                article.date = set.getString("uploadDate");
                article.title = set.getString("title");

                newsList.add(article);
            }

            return newsList;
        } catch (SQLException e) {
            logger.error(e.toString());
            logger.error("failed to get news");
            return null;
        }
    }

    public static UploadNewsArticleResponse createNewsArticle(UploadNewsArticleRequest body) {
        if (!UploadAccess.hasUploadAccess(body.token)){
            logger.error("no upload access");
            UploadNewsArticleResponse response = new UploadNewsArticleResponse();
            response.error = true;
            response.message = "No upload access";

            return response;
        } else {

            try {
                Statement st = connection.createStatement();


                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();

                String date = dtf.format(now);

                st.execute("insert into news(title, content, uploadDate) VALUES('" + body.title + "', '" + body.content +"', '" + date + "');");

                logger.info("inserted new news article");

                UploadNewsArticleResponse uploadNewsArticleResponse = new UploadNewsArticleResponse();
                uploadNewsArticleResponse.message = "Uploaded news article";
                uploadNewsArticleResponse.error = false;

                return uploadNewsArticleResponse;

            } catch (SQLException e) {
                logger.error(e.toString());
                logger.error("failed to upload news article");
                UploadNewsArticleResponse response = new UploadNewsArticleResponse();
                response.error = true;
                response.message = "SQLException during upload, check server logs for trace";
                return response;
            }
        }
    }

    public static String getLatestVersion() {
        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from latestVersion;");

            String latest = null;

            while (set.next()){
                latest = set.getString("version");
                break;
            }

            return latest;
        } catch (Exception e){
            logger.error(e.toString());
            logger.error("failed to query latest version");
            return null;
        }
    }

    public static boolean setLatestVersion(SetLatestVersionRequest body){
        if (!UploadAccess.hasUploadAccess(body.token)){
            return false;
        }

        if (!SqlUtils.isSqlInjectionSafe(body.latestVersion)){
            return false;
        }

        String latestVersion = getLatestVersion();

        try {
            Statement st = connection.createStatement();
            st.execute("update latestVersion set version='" + body.latestVersion + "' where version='" + latestVersion + "';");
            return true;
        } catch (Exception e){
            logger.error(e.toString());
            logger.error("failed to set latest version");
        }
        return false;
    }
}
