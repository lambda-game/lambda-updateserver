package com.interfiber.lambda.updater.server.requests;

public class SetLatestVersionRequest {
    public String latestVersion;
    public String token;
}
