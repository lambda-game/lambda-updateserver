package com.interfiber.lambda.updater.server.serverlist;

public class ServerInfo {
    public String serverName;
    public String serverDesc;
    public String serverIcon;
    public String serverIp;
}
