package com.interfiber.lambda.updater.server.security;

import com.interfiber.lambda.updater.server.SqlUtils;
import com.interfiber.lambda.updater.server.UpdateServerApplication;
import com.interfiber.lambda.updater.server.db.UpdateServerPG;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class UploadAccess {

    static Logger logger = LoggerFactory.getLogger(UpdateServerApplication.class);


    public static boolean hasUploadAccess(String token){
        boolean hasUploadPerms = false;

        if (!SqlUtils.isSqlInjectionSafe(token)){
            return false;
        }

        try {
            Statement st = UpdateServerPG.connection.createStatement();
            ResultSet set = st.executeQuery("select * from uploadTokens where token='" + token + "';");

            while (set.next()){
                if (Objects.equals(set.getString("token"), token)){
                    hasUploadPerms = true;
                    break;
                }
            }
            return hasUploadPerms;
        } catch (SQLException e){
            logger.error(e.toString());
            logger.error("failed to make upload token query");
        }

        return false;
    }
}