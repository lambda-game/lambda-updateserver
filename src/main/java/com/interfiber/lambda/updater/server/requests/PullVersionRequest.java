package com.interfiber.lambda.updater.server.requests;

public class PullVersionRequest {
    public String name;
    public String token;
}
