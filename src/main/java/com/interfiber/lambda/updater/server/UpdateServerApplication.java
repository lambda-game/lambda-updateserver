package com.interfiber.lambda.updater.server;

import com.interfiber.lambda.updater.server.db.UpdateServerPG;
import com.interfiber.lambda.updater.server.serverlist.ServerList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class UpdateServerApplication {

	public static void main(String[] args) {
		UpdateServerPG.init();
		UpdateServerPG.createDefaults();

		// clear server list every 30 seconds

		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
		scheduledExecutorService.scheduleWithFixedDelay(ServerList.serverInfoList::clear, 30, 30,
				TimeUnit.SECONDS);

		SpringApplication.run(UpdateServerApplication.class, args);
	}

}
