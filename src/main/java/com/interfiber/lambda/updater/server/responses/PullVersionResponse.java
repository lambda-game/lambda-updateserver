package com.interfiber.lambda.updater.server.responses;

public class PullVersionResponse {
    public String message;
    public boolean error;
}
