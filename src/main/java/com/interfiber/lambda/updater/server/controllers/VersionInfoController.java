package com.interfiber.lambda.updater.server.controllers;


import com.interfiber.lambda.updater.server.db.UpdateServerPG;
import com.interfiber.lambda.updater.server.db.VersionInfo;
import com.interfiber.lambda.updater.server.responses.LatestVersionResponse;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class VersionInfoController {
    @GetMapping("/api/v1/versions/list")
    public ArrayList<VersionInfo> getVersions(){
        ArrayList<VersionInfo> versionList  = UpdateServerPG.getVersionList();
        return versionList;
    }

    @GetMapping("/api/v1/versions/info/{name}")
    public VersionInfo getVersion(@PathVariable String name){
        return UpdateServerPG.getVersion(name);
    }

    @GetMapping("/api/v1/versions/latest")
    public LatestVersionResponse getLatest(){
        String latest = UpdateServerPG.getLatestVersion();

        LatestVersionResponse versionResponse = new LatestVersionResponse();
        versionResponse.latest = latest;
        return versionResponse;
    }
}
